import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import {AuthService} from '../core/service/auth.service';

import {Location} from '@angular/common';

import { TokenStorageService } from '../core/service/token-storage.service';


/**
 * Author: Andy.
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: any = {
    username: null,
    email: null,
    password: null,
    role: null
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  model = {
    checks: [
      {
        id: 'Restaurant owner',
        label: 'mod',
        selected: false,
      },
      {
        id: 'Administrator',
        label: 'admin',
        selected: false,
      },
    ],
  };

  constructor(private authService: AuthService,
              private location: Location, 
              private tokenStorage: TokenStorageService,
              private router: Router ) {
  }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      alert("You are already logged in, please log out to register. \nSending you back home! :)");
      this.router.navigateByUrl('/home');
    }
  }

  onSubmit(): void {
    const checkRole: string [] = [];
    this.model.checks.forEach(
      x => {
        if (x.selected === true) {
          checkRole.push(x.label);
        }
      }
    );
    if (checkRole.length !== 0) {
      this.form.role = checkRole;
    }
    const {username, email, password, role} = this.form;

    console.log('checkbox checks: ' + JSON.stringify(this.model.checks));
    console.log('checkbox roles: ' + JSON.stringify(this.form.role));

    this.authService.register(username, email, password, role).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.isSignUpFailed = false;

        alert("Register successful! You can log in now.")
        this.router.navigateByUrl('/login');
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
}
