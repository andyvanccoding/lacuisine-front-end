import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ReservationService} from '../core/service/reservation.service';
import {Location} from '@angular/common';
import {Reservation} from '../core/model/reservation';
import {Restaurant} from '../core/model/restaurant';
import {RestaurantService} from '../core/service/restaurant.service';
import {Reservationrequest} from '../core/model/reservationrequest';

/**
 * Created by "Andy Van Camp" on 26/08/2021.
 */
@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.css']
})
export class ReservationListComponent implements OnInit {

  reservationCollection: Reservation[];
  restaurant: Restaurant = new Restaurant();
  reservationRequest: Reservationrequest = new Reservationrequest();

  isLoaded = false;
  isSuccessful = false;

  errorMessage = '';

  constructor(private route: ActivatedRoute,
              private reservationService: ReservationService,
              private restaurantService: RestaurantService,
              private location: Location) {
  }

  ngOnInit(): void {
    const restaurantId = Number(this.route.snapshot.paramMap.get('id'));
    console.log('id from route: ' + restaurantId);

    this.restaurantService.getRestaurantById(restaurantId).subscribe(
      restaurant => {
        this.restaurant = restaurant;
      },
      error => {
        this.errorMessage = error.error;
      }
    );

    this.reservationService.getReservationByRestaurantId(restaurantId).subscribe(
      data => {
        this.reservationCollection = data.reservationCollection;
      },
      error => {
        this.errorMessage = error.error;
      },
      () => {
        this.isLoaded = true;
      }
    );

  }

  goBack(): void {
    this.location.back();
  }

  delete(id: number): void {
    this.reservationService.deleteReservationById(id).subscribe(
      value => {
      },
      error => {
        this.errorMessage = error.error;
      },
      () => {
        window.location.reload();
      }
    );
  }

  onSubmit(): void {
    this.downloadPdfList();
  }

  downloadPdfList(): void {
    this.reservationService
      .printReservationListPdf(Number(this.route.snapshot.paramMap.get('id')), this.reservationRequest).subscribe(data => {
      const binaryData = [];
      binaryData.push(data);
      const url = new URL('http://ListOfPdf');
      url.href = URL.createObjectURL(new Blob(binaryData, {
        type: 'application/zip'
      }));
      const fileLink = document.createElement('a');
      fileLink.href = url.href;
      fileLink.download = 'Reservation_List';
      fileLink.click();
      // window.open(url.href);
    });
  }
}
