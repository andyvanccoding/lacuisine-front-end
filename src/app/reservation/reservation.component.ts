import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Timeslotrequest} from '../core/model/timeslotrequest';
import {ReservationService} from '../core/service/reservation.service';
import {Timeslot} from '../core/model/timeslot';
import {Reservation} from '../core/model/reservation';
import {TokenStorageService} from '../core/service/token-storage.service';
import {Location} from '@angular/common';
import {RestauranttableService} from '../core/service/restauranttable.service';
import {Restauranttable} from '../core/model/restauranttable';

/**
 * Created by "Andy Van Camp" on 25/08/2021.
 */
@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {

  timeslotRequest: Timeslotrequest = new Timeslotrequest();

  savedReservation: Reservation;

  timeSlotMap = new Map<number, Timeslot[]>();

  tableMap = new Map<number, number>();
  tableCollection: Restauranttable[];

  username: string;
  restaurantname: string;

  isSuccessful: boolean;
  reserved = false;

  errorMessage = '';


  constructor(private route: ActivatedRoute,
              private reservationService: ReservationService,
              private tableService: RestauranttableService,
              private tokenStorage: TokenStorageService,
              private location: Location) {
  }

  ngOnInit(): void {
    this.username = this.tokenStorage.getUser().username;
    this.restaurantname = (this.route.snapshot.paramMap.get('name'));
  }

  onSubmit(): void {
    const restaurantId = Number(this.route.snapshot.paramMap.get('id'));
    console.log('id from route: ' + restaurantId);
    this.timeslotRequest.restaurant.id = restaurantId;
    console.log('date: ' + this.timeslotRequest.reservationDate);
    console.log('size: ' + this.timeslotRequest.partySize);
    this.reservationService.generateTimeslots(this.timeslotRequest).subscribe(
      data => {
        this.timeSlotMap = data;
      },
      error => {
        this.errorMessage = error.error.message;
      },
      () => {
        this.errorMessage = '';
        console.log('Timeslots: ' + JSON.stringify(this.timeSlotMap));
        for (const [key, value] of Object.entries(this.timeSlotMap)) {
          console.log(key, value);
        }
        this.mapTableSize();
      }
    );
  }

  private mapTableSize(): void {
    const restaurantId = Number(this.route.snapshot.paramMap.get('id'));
    this.tableService.getAllRestaurantTableByRestaurantId(restaurantId).subscribe(
      data => {
        this.tableCollection = data.restaurantTableCollection;
      },
      error => {
        this.errorMessage = error.error.message;
      },
      () => {
        this.mapTable();
        this.isSuccessful = true;
      }
    );
  }

  public getTableSize(key: any): number {
    key = Number(key);
    return this.tableMap.get(key);
  }

  private mapTable(): void {
    this.tableCollection.forEach(
      table => {
        this.tableMap.set(table.id, table.capacity);
      }
    );
  }

  makeReservation(startTime: string, endTime: string, table: number, available: boolean): void {
    if (!available) {
      alert('This timeslot is not available!');
    } else {
      if (confirm('are ya sure?')) {
        const reservation = new Reservation();
        reservation.restaurant.id = this.timeslotRequest.restaurant.id;
        reservation.startTime = startTime;
        reservation.endTime = endTime;
        reservation.restaurantTable.id = table;
        reservation.date = this.timeslotRequest.reservationDate;
        reservation.partySize = this.timeslotRequest.partySize;
        reservation.user.id = this.tokenStorage.getUser().id;
        this.reservationService.saveReservation(reservation).subscribe(
          data => {
            this.savedReservation = data;
          },
          error => {
            console.log('reached error');
            this.errorMessage = error.error.errorMessage;
          },
          () => {
            this.reserved = true;
          }
        );
      }
    }
  }

  backToDate(): void {
    this.isSuccessful = false;
    this.timeSlotMap = new Map<number, Timeslot[]>();
  }

  backToResto(): void {
    this.location.back();
  }


  downloadPdf(): void {
    this.reservationService.printReservationPdf(this.savedReservation.id).subscribe(data => {
      const binaryData = [];
      binaryData.push(data);
      const url = window.URL.createObjectURL(new Blob(binaryData, {
        type: 'application/pdf'
      }));
      window.open(url);
    });
  }

}
