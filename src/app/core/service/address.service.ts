/**
 * Created by "Andy Van Camp" on 10/08/2021.
 */
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Address} from '../model/address';

const API_URL = 'http://localhost:8080/api/address';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private httpClient: HttpClient) {
  }

  getAddressById(id: number): Observable<Address> {
    return this.httpClient.get<Address>(API_URL + '/' + id);
  }

  saveAddress(address: Address): Observable<Address> {
    return this.httpClient.post<Address>(API_URL, address, httpOptions);
  }

  updateAddress(address: Address): Observable<Address> {
    return this.httpClient.put<Address>(API_URL + '/' + address.id, address, httpOptions);
  }
}
