/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {WalletCollection} from '../model/wallet-collection';
import {Wallet} from '../model/wallet';

const API_URL = 'http://localhost:8080/api/wallet';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class WalletService {


  constructor(private httpClient: HttpClient) {
  }

  public getAllWallets(): Observable<WalletCollection> {
    return this.httpClient.get<WalletCollection>(API_URL);
  }

  public getWalletById(id: number): Observable<Wallet> {
    return this.httpClient.get<Wallet>(API_URL + '/' + id);
  }
// update Wallet
  public addToWallet(id: number, value: number): Observable<any> {
    console.log('credits to wallet: id= ' + id + ' value= ' + value);
    return this.httpClient.put(API_URL + '/add?id=' + id + '&value=' + value, {}, httpOptions);
  }

}
