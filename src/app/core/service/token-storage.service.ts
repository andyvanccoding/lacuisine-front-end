import {Injectable} from '@angular/core';
/**
 * Author: Andy.
 */
/*
TokenStorageService to manages token and user information (username, email, roles)
inside Browser’s Session Storage.
For Logout, we only need to clear this Session Storage.

validate in the form:

  username: required
  password: required, minLength=6
*/

const TOKEN_KEY = 'auth-token'; // jwt
const USER_KEY = 'auth-user';

// const ORDER_KEY = 'order-access-key';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  constructor() {
  }

  signOut(): void {
    window.sessionStorage.clear();
  }

  public saveToken(token: string): void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string | null {
    return window.sessionStorage.getItem(TOKEN_KEY);
  }

  public saveUser(user: any): void {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser(): any {
    const user = window.sessionStorage.getItem(USER_KEY);
    if (user) {
      return JSON.parse(user);
    }

    return {};
  }
}
