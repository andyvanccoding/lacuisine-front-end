import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {OpeninghoursCollection} from '../model/openinghours-collection';
import {Openinghours} from '../model/openinghours';

/**
 * Created by "Andy Van Camp" on 17/08/2021.
 */
const API_URL = 'http://localhost:8080/api/openinghours';
const httpOptions = {
  headers: new HttpHeaders({'content-type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class OpeninghoursService {

  constructor(private httpClient: HttpClient) {
  }

  getAllOpeninghours(): Observable<OpeninghoursCollection> {
    return this.httpClient.get<OpeninghoursCollection>(API_URL);
  }

  getAllOpeninghoursByRestaurantId(restauranId: number): Observable<OpeninghoursCollection> {
    return this.httpClient.get<OpeninghoursCollection>(API_URL + '/?restaurantId=' + restauranId);
  }

  saveOpeninghours(openinghours: Openinghours): Observable<Openinghours> {
    return this.httpClient.post<Openinghours>(API_URL, openinghours, httpOptions);
  }

  updateOpeninghours(openinghours: Openinghours): Observable<Openinghours> {
    return this.httpClient.put<Openinghours>(API_URL, openinghours, httpOptions);
  }

  deleteOpeninghoursById(id: number): Observable<any> {
    return this.httpClient.delete(API_URL + '/' + id);
  }
}
