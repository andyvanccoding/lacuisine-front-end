/**
 * Created by "Dorien and Nick" on 20/08/2021.
 */

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Rating } from '../model/rating';
import { RatingCollection } from '../model/rating-collection';

const API_URL = 'http://localhost:8080/api/rating';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class RatingService {

  constructor(private httpClient: HttpClient) { }

  getRatingByRestaurantId(restaurantId: number): Observable<RatingCollection> {
    return this.httpClient.get<RatingCollection>(API_URL + '?restaurantId=' + restaurantId);
  }

  getRatingByUserId(userId: number): Observable<RatingCollection> {
    return this.httpClient.get<RatingCollection>(API_URL + '?userId=' + userId);
  }

  saveRating(rating: Rating): Observable<Rating> {
    return this.httpClient.post<Rating>(API_URL, rating, httpOptions);
  }

  deleteRating(id: number): Observable<any>{
    return this.httpClient.delete(API_URL + '/' + id);
  }
}
