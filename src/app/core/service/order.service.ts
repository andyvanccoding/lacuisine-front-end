/**
 * shiwi & oli
 * Dorien and Nick
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Order} from '../model/order';
import {OrderItem} from '../model/orderItem';
import {OrderCollection} from '../model/order-collection';

const API_URL = 'http://localhost:8080/api/order';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  constructor(private httpClient: HttpClient) {
  }

//  oItems: OrderItem [] = [];
  // addToOrder(orderItem: OrderItem) {
  //  this.oItems.push(orderItem);
  // }
  getOrderById(id: number): Observable<Order> {
    return this.httpClient.get<Order>(API_URL + '/' + id);
  }

  upDateOrder(id: number, order1: Order): Observable<Order> {
    return this.httpClient.put<Order>(API_URL + '/' + id, order1, httpOptions);
  }

  deleteOrderById(id: number): Observable<any> {
    return this.httpClient.delete(API_URL + '/' + id);
  }

  getAllOrder(): Observable<OrderCollection> {
    return this.httpClient.get<OrderCollection>(API_URL);
  }

  saveOrder(order: Order): Observable<Order> {
    return this.httpClient.post<Order>(API_URL, order, httpOptions);
  }

  getOrderByRestaurantId(restaurantId: number): Observable<OrderCollection> {
    return this.httpClient.get<OrderCollection>(API_URL + '?restaurantId=' + restaurantId);
  }

  getOrderByUserId(userId: number): Observable<OrderCollection> {
    return this.httpClient.get<OrderCollection>(API_URL + '?userId=' + userId);
  }
}
