/**
 * shiwi & oli
 */

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Menu} from '../model/menu';
const API_URL = 'http://localhost:8080/api/menu';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private httpClient: HttpClient) {
  }
  getMenuId(id: number): Observable<Menu>{
    return this.httpClient.get<Menu>(API_URL);
  }
  updateMenu(id: number, menu1: Menu): Observable<Menu>{
    return this.httpClient.put<Menu>(API_URL, menu1, httpOptions);
  }
}
