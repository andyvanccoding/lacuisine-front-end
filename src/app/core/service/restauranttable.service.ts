import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Restauranttable} from '../model/restauranttable';
import {RestauranttableCollection} from '../model/restauranttable-collection';

/**
 * Created by "Andy Van Camp" on 19/08/2021.
 */

const API_URL = 'http://localhost:8080/api/restauranttable';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class RestauranttableService {


  constructor(private httpClient: HttpClient) {
  }

  getRestaurantTableById(id: number): Observable<Restauranttable> {
    return this.httpClient.get<Restauranttable>(API_URL + '/' + id);
  }

  getAllRestaurantTable(): Observable<RestauranttableCollection> {
    return this.httpClient.get<RestauranttableCollection>(API_URL);
  }

  getAllRestaurantTableByRestaurantId(restaurantId: number): Observable<RestauranttableCollection> {
    return this.httpClient.get<RestauranttableCollection>(API_URL + '?restaurantId=' + restaurantId);
  }

  saveRestaurantTable(restaurantTable: Restauranttable): Observable<Restauranttable> {
    return this.httpClient.post<Restauranttable>(API_URL, restaurantTable, httpOptions);
  }

  updateRestaurantTable(restaurantTable: Restauranttable): Observable<Restauranttable> {
    return this.httpClient.put<Restauranttable>(API_URL, restaurantTable, httpOptions);
  }

  deleteRestaurantById(id: number): Observable<any> {
    return this.httpClient.delete(API_URL + '/' + id);
  }

}
