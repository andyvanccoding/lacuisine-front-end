import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ReservationCollection} from '../model/reservation-collection';
import {Timeslotrequest} from '../model/timeslotrequest';
import {Reservation} from '../model/reservation';
import {Timeslot} from '../model/timeslot';
import {Reservationrequest} from '../model/reservationrequest';

/**
 * Created by "Andy Van Camp" on 25/08/2021.
 */

const API_URL = 'http://localhost:8080/api/reservation';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private httpClient: HttpClient) {
  }

  getAllReservation(): Observable<ReservationCollection> {
    return this.httpClient.get<ReservationCollection>(API_URL);
  }

  getReservationByUserId(userId: number): Observable<ReservationCollection> {
    return this.httpClient.get<ReservationCollection>(API_URL + '?userId=' + userId);
  }

  getReservationByRestaurantId(restaurantId: number): Observable<ReservationCollection> {
    return this.httpClient.get<ReservationCollection>(API_URL + '?restaurantId=' + restaurantId);
  }

  generateTimeslots(timeslotRequest: Timeslotrequest): Observable<Map<number, Timeslot[]>> {
    return this.httpClient.post<Map<number, Timeslot[]>>(API_URL + '/timeslots', timeslotRequest, httpOptions);
  }

  saveReservation(reservation: Reservation): Observable<Reservation> {
    return this.httpClient.post<Reservation>(API_URL, reservation, httpOptions);
  }

  deleteReservationById(id: number): Observable<any> {
    return this.httpClient.delete(API_URL + '/' + id);
  }

  printReservationPdf(id: number): Observable<ArrayBuffer> {
    return this.httpClient.get(API_URL + '/reservationPdf/' + id, {responseType: 'arraybuffer'});
  }

  printReservationListPdf(id: number, reservationRequest: Reservationrequest): Observable<ArrayBuffer> {
    return this.httpClient.post(API_URL + '/reservationPdfList/' + id, reservationRequest, {responseType: 'arraybuffer'});
  }

}
