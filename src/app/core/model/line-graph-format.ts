class MenuItemDataLine {
  name: string;
  value: number;
}

/**
 *  Created by "Andy" on 19/12/2021.
 */

export class LineGraphFormat {
  name: string;
  series: MenuItemDataLine[];
}
