/**
 * Dorien and Nick 20/08/2021.
 */

import { Rating } from "./rating";

export class RatingCollection {
    ratingCollection: Rating[] = [];
}
