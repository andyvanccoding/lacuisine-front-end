import {Restaurant} from './restaurant';
import {User} from './user';
import {Restauranttable} from './restauranttable';

/**
 * Created by "Andy Van Camp" on 25/08/2021.
 */
export class Reservation {
  id: number;
  restaurant: Restaurant = new Restaurant();
  user: User = new User();
  date: string;
  startTime: string;
  endTime: string;
  partySize: number;
  restaurantTable: Restauranttable = new Restauranttable();
}
