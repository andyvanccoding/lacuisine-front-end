import {Restaurant} from './restaurant';

/**
 * Created by "Andy Van Camp" on 17/08/2021.
 */
export class Openinghours {
  id: number;
  openTime: string;
  closeTime: string;
  weekday: string;
  restaurant: Restaurant;
  reservationTimeslot: number;
  active: boolean;
}
