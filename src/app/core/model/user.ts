/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
import {Role} from './role';

export class User {
  id: number;
  username: string;
  email: string;
  password: string;
  roles: Role[];
  storeCredit: number;
}
