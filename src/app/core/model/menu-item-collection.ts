/**
 *  Created by "Dorien and Nick" on 12/08/2021.
 */

import { MenuItem } from './menuItem';

export class MenuItemCollection {
    menuItemCollection: MenuItem[];
}
