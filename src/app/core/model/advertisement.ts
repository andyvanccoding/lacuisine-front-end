import {Restaurant} from './restaurant';

export class Advertisement {
  id: number;
  description: string;
  creation: Date;
  expireDate: Date;
  restaurant: Restaurant;
}
