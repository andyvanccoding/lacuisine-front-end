/**
 * Shiwi & Oli
 */
import {Menu} from './menu';


export class MenuItem{
  id: number;
  price: number;
  foodType: string;
  vegetarian: boolean;
  name: string;
  menu: Menu;
}
