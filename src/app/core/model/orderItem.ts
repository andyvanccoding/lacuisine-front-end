/**
 * Shiwi & Oli
 */

import {MenuItem} from './menuItem';
import {Order} from './order';

export class OrderItem{
  id: number;
  menuItem: MenuItem = new MenuItem();
  order: Order = new Order();
  amount: number;
}
