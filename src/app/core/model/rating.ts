/**
 * Dorien & Nick 20/08/2021
 */

import { Restaurant } from "./restaurant";
import { User } from "./user";

export class Rating {
    id: number;
    restaurant: Restaurant = new Restaurant();
    score: number;
    feedback: string;
    date: Date;
    user: User = new User();
}
