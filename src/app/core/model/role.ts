/**
 * Created by "Andy Van Camp" on 9/08/2021.
 */
export class Role {
  id: number;
  name: string;
}
