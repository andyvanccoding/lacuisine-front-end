import {Restaurant} from './restaurant';

/**
 * Created by "Andy Van Camp" on 25/08/2021.
 */
export class Timeslotrequest {
  reservationDate: string;
  partySize: number;
  restaurant: Restaurant = new Restaurant();
}
