/**
 * Created by "Andy Van Camp" on 10/08/2021.
 */
import {Restaurant} from './restaurant';

export class RestaurantCollection {
  restaurantCollection: Restaurant[];
}
