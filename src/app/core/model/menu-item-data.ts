export class MenuItemData {
  name: string;
  foodType: string;
  vegetarian: boolean;
  amount: number;
  price: number;
  orderDate: string;
}
