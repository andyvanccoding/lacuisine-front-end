import { MenuItem } from './menuItem';
import { Restaurant } from './restaurant';

export class Menu {
    id: number;
    name: string;
    menuItemList: MenuItem[];
    restaurant: Restaurant;
}
