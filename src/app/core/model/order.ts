/**
 * Shiwi & Oli
 */
import {User} from './user';
import {Restaurant} from './restaurant';
import DateTimeFormat = Intl.DateTimeFormat;
import {OrderItem} from './orderItem';

export class Order {
  id: number;
  user: User = new User();
  restaurant: Restaurant = new Restaurant();
  orderTime: Date;
  orderItemList: OrderItem[];
  totalPrice: number;
}
