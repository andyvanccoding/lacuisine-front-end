import {Restaurant} from './restaurant';

export class Restauranttable {
  id: number;
  capacity: number;
  restaurant: Restaurant;
  active: boolean;
}
