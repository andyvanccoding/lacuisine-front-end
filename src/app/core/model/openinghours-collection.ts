import {Openinghours} from './openinghours';

/**
 * Created by "Andy Van Camp" on 17/08/2021.
 */
export class OpeninghoursCollection {
  openinghoursCollection: Openinghours[];
}
