import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../core/service/token-storage.service';
import {RestaurantService} from '../core/service/restaurant.service';
import {Restaurant} from '../core/model/restaurant';
import {WalletService} from '../core/service/wallet.service';
import {RatingService} from '../core/service/rating.service';
import {RatingCollection} from '../core/model/rating-collection';
import {ReservationService} from '../core/service/reservation.service';
import {Reservation} from '../core/model/reservation';
/**
 * Author: Andy.
 */

/*
* This Component gets current User from Storage using TokenStorageService and
* show information (username, token, email, roles).
* */

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  currentUser: any;

  currentUserCredit: number;

  restaurantCollection: Restaurant[];

  reservationCollection: Reservation[];

  errorMessage = '';

  imgSource = 'assets/img/default.jpg';

  showAddResto = false;
  showReservation = false;

  ratingCollection: RatingCollection = new RatingCollection();
  rating: number;
  childActive = false;

  constructor(private token: TokenStorageService,
              private restaurantervice: RestaurantService,
              private walletService: WalletService,
              private ratingService: RatingService,
              private reservationService: ReservationService) {
  }

  ngOnInit(): void {
    this.currentUser = this.token.getUser();
    this.restaurantervice.getAllRestaurantByUserId(this.currentUser.id).subscribe(
      restaurantCollection => {
        this.restaurantCollection = restaurantCollection.restaurantCollection;
        this.restaurantCollection.forEach(restaurant => console.log('restaurant found: ' + JSON.stringify(restaurant)));
      },
      error => {
        this.errorMessage = error.message.error;
      },
      () => {
        this.showAddResto = this.currentUser.roles.includes('ROLE_ADMIN');
        if (!this.showAddResto) {
          this.showAddResto = this.currentUser.roles.includes('ROLE_MODERATOR');
        }
        this.restaurantCollection.forEach(restaurant => this.showRestaurantRating(restaurant));
      }
    );

    this.walletService.getWalletById(this.currentUser.id).subscribe(
      wallet => {
        this.currentUserCredit = wallet.storeCredit;
      },
      error => {
        this.errorMessage = error.message.error;
      }
    );
  }

  showRestaurantRating(restaurant: Restaurant): void {
    this.ratingService.getRatingByRestaurantId(restaurant.id).subscribe(
      data => {
        this.ratingCollection = data;
      },
      error => {
        this.errorMessage = error.message.error;
      },
      () => {
        if (this.ratingCollection.ratingCollection.length !== 0) {
          this.rating = 0;
          this.ratingCollection.ratingCollection.forEach(rating => this.rating = (this.rating + rating.score));
          this.rating = this.rating / this.ratingCollection.ratingCollection.length;
        } else {
          this.rating = 0;
        }
        restaurant.rating = this.rating;
      }
    );
  }

  getReservation(): void {
    if (this.showReservation) {
      this.showReservation = false;
    } else {
      this.reservationService.getReservationByUserId(this.currentUser.id).subscribe(
        data => {
          this.reservationCollection = data.reservationCollection;
        },
        error => {
          this.errorMessage = error.message.error;
        },
        () => {
          this.showReservation = true;
        }
      );
    }
  }

  setChildActive(): void {
    this.childActive = true;
  }

  setChildInActive(): void {
    this.childActive = false;
  }
}
