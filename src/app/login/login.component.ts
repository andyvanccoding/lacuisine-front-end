import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../core/service/token-storage.service';
import {AuthService} from '../core/service/auth.service';
import {Location} from '@angular/common';

/**
 * Author: Andy.
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {
    username: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  name = '';

  constructor(private authService: AuthService,
              private tokenStorage: TokenStorageService,
              private location: Location) {
  }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
      this.name = this.tokenStorage.getUser().username;
    }
  }

  onSubmit(): void {
    const {username, password} = this.form;

    this.authService.login(username, password).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken); // jwt
        this.tokenStorage.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        const user = this.tokenStorage.getUser();
        this.roles = user.roles;
        // this.reloadPage(); to check roles
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      },
      () => {
        this.location.go('/profile');
        window.location.reload();
      }
    );
  }

  reloadPage(): void {
    window.location.reload();
  }

}
