/**
 * Created by "Andy Van Camp".
 */
import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../core/service/token-storage.service';
import {User} from '../core/model/user';
import {UserService} from '../core/service/user.service';
import {Wallet} from '../core/model/wallet';
import {WalletService} from '../core/service/wallet.service';

@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.css']
})
export class BoardAdminComponent implements OnInit {

  form: any = {
    id: null,
    value: null
  };

  loggedUser: User = new User();

  wallets: Wallet[] = [];
  users: User[] = [];

  userListFinish: User[] = [];

  errorMessage = '';
  isAddFailed = false;

  constructor(private walletService: WalletService,
              private userService: UserService,
              private tokenStorage: TokenStorageService) {
  }

  ngOnInit(): void {
    this.getWalletCollection();
    this.loggedUser = this.tokenStorage.getUser();
  }

  private getWalletCollection(): void {
    this.walletService.getAllWallets().subscribe(
      walletCollection => {
        this.wallets = walletCollection.walletCollection;
        this.wallets.forEach(wallet => console.log('wallets found: ' + JSON.stringify(wallet)));

      },
      error => {
        'whatever';
      },
      () => {
        this.getUserCollection();
      }
    );
  }

  private getUserCollection(): void {
    this.userService.getAllUsers().subscribe(
      userCollection => {
        this.users = userCollection.userCollection;
        this.users.forEach(user => console.log('users found: ' + JSON.stringify(user)));
      },
      error => {
        'whatever';
      },
      () => {
        this.combineDataUserWallet();
      }
    );
  }

  private combineDataUserWallet(): void {
    this.users.forEach(user => {
      this.wallets.forEach(wallet => {
        if (wallet.id === user.id) {
          user.storeCredit = wallet.storeCredit;
        }
      });
      this.userListFinish.push(user);
    });
    this.userListFinish.forEach(user => console.log('combined: ' + JSON.stringify(user))
    );
  }

  onSubmit(): void {
    const {id, value} = this.form;
    this.walletService.addToWallet(id, value).subscribe(
      () => {
        window.location.reload();
      },
      error => {
        this.errorMessage = error.error.message;
        this.isAddFailed = true;
      }
    );
  }

  selectUser(id: number): void {
    this.form.id = id;
  }


  deleteUser(id: number): void {
    this.userService.deleteUser(id).subscribe(
      () => {
      },
      error => {
        'whatever';
        // todo write error for deleting user
      },
      () => {
        // this.combined = this.combined.filter(user => user.id === id);
        window.location.reload();
      }
    );
  }
}
