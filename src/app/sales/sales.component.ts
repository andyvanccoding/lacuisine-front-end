import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {MenuItemService} from '../core/service/menu-item.service';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})
export class SalesComponent implements OnInit, OnChanges {

  resultDataPie: any[] = [];
  // resultDataLine: any[] = [];
  resultDataLine: any[];

  errorMessage = '';

  pieLoaded = false;
  lineLoaded = false;
  // options pie chart
  viewPie: any[] = [700, 400];
  gradient: boolean = true;
  showLegend: boolean = true;
  isDoughnut: boolean = false;
  colorSchemePie = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  // options line chart
  view: any[] = [800, 800];
  legend = true;
  showLabels = true;
  animations = true;
  xAxis = true;
  yAxis = true;
  showYAxisLabel = true;
  showXAxisLabel = true;
  xAxisLabel = 'Day';
  yAxisLabel = 'Total Sales';
  timeline = true;

  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };

  constructor(private route: ActivatedRoute,
              private location: Location,
              private menuItemService: MenuItemService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    throw new Error('Method not implemented.');
  }

  ngOnInit(): void {

  }

  loadPieChartTotal(): void {
    this.lineLoaded = false;
    this.pieLoaded = false;
    this.resultDataPie = [];
    const restaurantId = Number(this.route.snapshot.paramMap.get('id'));
    this.menuItemService.findPieChartGrapDataByRestaurantId(restaurantId, 'total').subscribe(
      data => {
        console.log(data);
        this.resultDataPie = data;
      },
      error => {
        this.errorMessage = error.error;
      },
      () => {
        this.pieLoaded = true;
      }
    );
  }

  loadPieChartFood(): void {
    this.lineLoaded = false;
    this.pieLoaded = false;
    this.resultDataPie = [];
    const restaurantId = Number(this.route.snapshot.paramMap.get('id'));
    this.menuItemService.findPieChartGrapDataByRestaurantId(restaurantId, 'food').subscribe(
      data => {
        console.log(data);
        this.resultDataPie = data;
      },
      error => {
        this.errorMessage = error.error;
      },
      () => {
        this.pieLoaded = true;
      }
    );
  }

  loadPieChartDrink(): void {
    this.lineLoaded = false;
    this.pieLoaded = false;
    this.resultDataPie = [];
    const restaurantId = Number(this.route.snapshot.paramMap.get('id'));
    this.menuItemService.findPieChartGrapDataByRestaurantId(restaurantId, 'drink').subscribe(
      data => {
        console.log(data);
        this.resultDataPie = data;
      },
      error => {
        this.errorMessage = error.error;
      },
      () => {
        this.pieLoaded = true;
      }
    );
  }

  loadFoodDataForLine(): void {
    this.lineLoaded = false;
    this.pieLoaded = false;
    this.resultDataLine = [];
    const restaurantId = Number(this.route.snapshot.paramMap.get('id'));
    this.menuItemService.findLineGrapDataByRestaurantId(restaurantId).subscribe(
      data => {
        console.log(data);
        this.resultDataLine = data;
      },
      error => {
        this.errorMessage = error.error;
      },
      () => {
        this.lineLoaded = true;
      }
    );
  }
}
