import {ComponentFixture, TestBed} from '@angular/core/testing';

import {OpeninghoursEditComponent} from './openinghours-edit.component';

describe('OpeninghoursEditComponent', () => {
  let component: OpeninghoursEditComponent;
  let fixture: ComponentFixture<OpeninghoursEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpeninghoursEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpeninghoursEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
