/**
 * Created by "Dorien and Nick" on 23/08/2021.
 */

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Order} from '../../core/model/order';
import {Restaurant} from '../../core/model/restaurant';
import {OrderItemService} from '../../core/service/order-item.service';
import {OrderService} from '../../core/service/order.service';
import {concatMap, forkJoin, map} from 'rxjs';

@Component({
  selector: 'app-restaurant-orders',
  templateUrl: './restaurant-orders.component.html',
  styleUrls: ['./restaurant-orders.component.css']
})
export class RestaurantOrdersComponent implements OnInit {
  restaurant: Restaurant = new Restaurant();

  orders: Order[];
  totalEarnings = 0;

  errorMessage: '';

  constructor(private orderService: OrderService,
              private orderItemService: OrderItemService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    const restaurantId = Number(this.route.snapshot.paramMap.get('id'));
    const observable = this.orderService.getOrderByRestaurantId(restaurantId).pipe(
      map(t => t.orderCollection),
      map(data => {
        return data.map(dat => this.orderItemService.getOrderItemsByOrderId(dat.id).pipe(
            map(res => {
              dat.orderItemList = res.orderItemCollection;
              return dat;
            })
          )
        );
      }),
      concatMap(arrayOfObservables => forkJoin(arrayOfObservables)
      ));
    observable.subscribe({
      next: value => this.orders = value,
      complete: () => this.calculateOrderItems()
    });
  }

  calculateOrderItems(): void {
    let totalPriceTemp = 0;
    this.orders.forEach(order => {
      order.orderItemList.forEach(orderItem => totalPriceTemp += (orderItem.menuItem.price * orderItem.amount));
      order.totalPrice = totalPriceTemp;
      this.totalEarnings += totalPriceTemp;
    });
  }
}
