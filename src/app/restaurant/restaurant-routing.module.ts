import {NgModule} from '@angular/core';
import {RestaurantComponent} from './restaurant.component';
import {RestaurantDetailComponent} from './restaurant-detail/restaurant-detail.component';
import {RestaurantOwnerGuardForChild} from '../shared/guards/restaurant-owner-guard-for-child.service';
import {RestaurantEditComponent} from './restaurant-edit/restaurant-edit.component';
import {RestauranttableEditComponent} from './restauranttable-edit/restauranttable-edit.component';
import {OpeninghoursEditComponent} from './openinghours-edit/openinghours-edit.component';
import {AdvertisementEditComponent} from './advertisement-edit/advertisement-edit.component';
import {MenuItemEditComponent} from './menu-item-edit/menu-item-edit.component';
import {ReservationComponent} from '../reservation/reservation.component';
import {RestaurantOrdersComponent} from './restaurant-orders/restaurant-orders.component';
import {RestaurantOwnerGuard} from '../shared/guards/restaurant-owner-guard.service';
import {SalesComponent} from '../sales/sales.component';
import {ReservationListComponent} from '../reservation-list/reservation-list.component';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [],
    component: RestaurantComponent,
    children: [
      {
        path: ':id',
        component: RestaurantDetailComponent,
        canActivateChild: [RestaurantOwnerGuardForChild],
        children: [
          {path: 'edit', component: RestaurantEditComponent},
          {path: 'edit/table', component: RestauranttableEditComponent},
          {path: 'edit/openinghours', component: OpeninghoursEditComponent},
          {path: 'edit/advertisement', component: AdvertisementEditComponent},
          {path: 'edit/menu', component: MenuItemEditComponent},

        ]
      },
      {path: ':id/reservation/:name', component: ReservationComponent, canActivate: []},
      {path: ':id/orders', component: RestaurantOrdersComponent, canActivate: [RestaurantOwnerGuard]},
      {path: ':id/sales', component: SalesComponent, canActivate: [RestaurantOwnerGuard]},
      {path: ':id/reservations', component: ReservationListComponent, canActivate: [RestaurantOwnerGuard]},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestaurantRoutingModule {
}
