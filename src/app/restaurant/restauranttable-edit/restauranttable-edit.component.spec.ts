import {ComponentFixture, TestBed} from '@angular/core/testing';

import {RestauranttableEditComponent} from './restauranttable-edit.component';

describe('RestauranttableEditComponent', () => {
  let component: RestauranttableEditComponent;
  let fixture: ComponentFixture<RestauranttableEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RestauranttableEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestauranttableEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
