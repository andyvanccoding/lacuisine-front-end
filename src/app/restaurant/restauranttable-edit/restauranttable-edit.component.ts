import {Component, OnInit} from '@angular/core';
import {Restauranttable} from '../../core/model/restauranttable';
import {RestauranttableService} from '../../core/service/restauranttable.service';
import {ActivatedRoute} from '@angular/router';
import {Restaurant} from '../../core/model/restaurant';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {Observable} from "rxjs";


/**
 * Created by "Andy Van Camp" on 19/08/2021.
 */
@Component({
  selector: 'app-restauranttable-edit',
  templateUrl: './restauranttable-edit.component.html',
  styleUrls: ['./restauranttable-edit.component.css']
})
export class RestauranttableEditComponent implements OnInit {

  tableForm: FormGroup;

  restaurantTable: Restauranttable = new Restauranttable();
  restaurantTableCollection: Restauranttable[];

  restaurant: Restaurant = new Restaurant();

  isSuccessful = false;
  isFailed = false;
  errorMessage = '';
  error = false;
  isNew = true;
  isLoaded = false;

  constructor(private tableService: RestauranttableService,
              private location: Location,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {

    this.tableForm = new FormGroup({
      'capacity': new FormControl(null, [Validators.required, Validators.min(1)])
    });

    this.restaurant.id = Number(this.route.snapshot.paramMap.get('id'));
    this.tableService.getAllRestaurantTableByRestaurantId(this.restaurant.id).subscribe({
      next: data => this.restaurantTableCollection = data.restaurantTableCollection,
      error: err => this.errorMessage = err.error.errorMessage,
      complete: () => this.isLoaded = true
    });
  }

  onSubmit(): void {
    console.log(this.tableForm);
    if (+this.tableForm.get('capacity').value < 1) {
      this.isFailed = true;
      this.errorMessage = 'capacity must be higher then 0';
    } else {
      if (confirm('wanne save?')) {
        if (this.restaurantTable.id === undefined) {
          this.restaurantTable.restaurant = this.restaurant;
          this.restaurantTable.capacity = (+this.tableForm.get('capacity').value);
          this.tableService.saveRestaurantTable(this.restaurantTable).subscribe(
            value => {
              console.log(this.tableForm);
            },
            error1 => {
              this.errorMessage = error1.error.errorMessage;
            },
            () => {
              window.location.reload();
              this.isSuccessful = true;
            }
          );
        } else {
          console.log(+this.tableForm.get('capacity'));
          this.restaurantTable.capacity = (+this.tableForm.get('capacity').value);
          this.tableService.updateRestaurantTable(this.restaurantTable).subscribe(
            value => {
            },
            error1 => {
              this.errorMessage = error1.error.errorMessage;
            },
            () => {
              window.location.reload();
              this.isSuccessful = true;
            }
          );
        }
      }
    }

  }

  setNew(): void {
    this.restaurantTable = new Restauranttable();
    this.isSuccessful = false;
    this.isNew = true;
  }

  goBack(): void {
    this.location.back();
  }

  deleteRestaurantTable(): void {
    this.tableService.deleteRestaurantById(this.restaurantTable.id).subscribe({
      complete: () => window.location.reload()
    });
  }

  setForUpdate(restaurantTable: Restauranttable): void {
    this.restaurant = restaurantTable.restaurant;
    this.isNew = false;
    this.restaurantTable = restaurantTable;
    this.tableForm.setValue({capacity: restaurantTable.capacity});
  }
}
