/**
 * Created by "Andy Van Camp" on 10/08/2021.
 */
import {Component, OnInit} from '@angular/core';
import {RestaurantService} from '../core/service/restaurant.service';
import {Restaurant} from '../core/model/restaurant';
import {OpeninghoursService} from '../core/service/openinghours.service';
import {Openinghours} from '../core/model/openinghours';
import {CurrentDay} from '../core/model/current-day';
import {RatingService} from '../core/service/rating.service';
import {RatingCollection} from '../core/model/rating-collection';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.css']
})
export class RestaurantComponent implements OnInit {

  childIsActive = false;

  restaurantCollection: Restaurant[];

  openhoursCollection: Openinghours[];

  restaurantCollectionOpen: Restaurant[] = [];

  imgSource = 'assets/img/default.jpg';

  errorMessage = '';

  days = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];

  ratingCollection: RatingCollection = new RatingCollection();
  rating: number;

  constructor(private restaurantService: RestaurantService,
              private openhoursService: OpeninghoursService,
              private ratingService: RatingService
  ) {
  }

  ngOnInit(): void {
    this.restaurantService.getAllRestaurant().subscribe(
      restaurantCollection => {
        this.restaurantCollection = restaurantCollection.restaurantCollection;
      },
      error => {
        this.errorMessage = error.error.errorMessage;
      },
      () => {
        this.openhoursService.getAllOpeninghours().subscribe(
          data => {
            this.openhoursCollection = data.openinghoursCollection;
          },
          error => {
            this.errorMessage = error.error.errorMessage;
          },
          () => {
            this.searchOpen();
            this.restaurantCollection.forEach(restaurant => {
              this.showRestaurantRating(restaurant);
            });
            this.searchClose();
          }
        );
      }
    );
  }

  // if no open or close time is set not shown!!
  searchOpen(): void {
    const currentDay: CurrentDay = new CurrentDay();
    const date = new Date();
    currentDay.day = this.days[date.getDay()];
    currentDay.time = date.toLocaleTimeString();

    this.restaurantCollection.forEach(
      restaurant => {
        this.openhoursCollection.forEach(
          openinghour => {
            if (restaurant.id === openinghour.restaurant.id) {
              if (
                openinghour.active &&
                openinghour.openTime <= currentDay.time &&
                openinghour.closeTime >= currentDay.time &&
                openinghour.weekday === currentDay.day) {
                restaurant.open = true;
                if (!this.restaurantCollectionOpen.includes(restaurant)) {
                  this.restaurantCollectionOpen.push(restaurant);
                  const index = this.restaurantCollection.indexOf(restaurant);
                  this.restaurantCollection.splice(index, 1, new Restaurant());
                }
              }
            }
          }
        );
      }
    );
  }

  searchClose(): void {
    const currentDay: CurrentDay = new CurrentDay();
    const date = new Date();
    currentDay.day = this.days[date.getDay()];
    currentDay.time = date.toLocaleTimeString();

    this.restaurantCollection.forEach(
      restaurant => {
        this.openhoursCollection.forEach(
          openinghour => {
            if (restaurant.id === openinghour.restaurant.id) {
              if (
                openinghour.active
              ) {
                restaurant.open = false;
                if (!this.restaurantCollectionOpen.includes(restaurant)) {
                  this.restaurantCollectionOpen.push(restaurant);
                  const index = this.restaurantCollection.indexOf(restaurant);
                  this.restaurantCollection.splice(index, 1, new Restaurant());
                }
              }
            }
          }
        );
      }
    );
  }

  removeOpenFromCollection(): void {
    this.restaurantCollectionOpen.forEach(
      restaurant => {
        if (this.restaurantCollection.includes(restaurant)) {
          const index = this.restaurantCollection.indexOf(restaurant);
          this.restaurantCollection.splice(index, 1);
        }
      }
    );
  }

  showRestaurantRating(restaurant: Restaurant): void {
    this.ratingService.getRatingByRestaurantId(restaurant.id).subscribe(
      data => {
        this.ratingCollection = data;
      },
      error => {
        this.errorMessage = error.message.error;
      },
      () => {
        if (this.ratingCollection.ratingCollection.length !== 0) {
          this.rating = 0;
          this.ratingCollection.ratingCollection.forEach(rating => this.rating = (this.rating + rating.score));
          this.rating = this.rating / this.ratingCollection.ratingCollection.length;
        } else {
          this.rating = 0;
        }
        restaurant.rating = this.rating;
      }
    );
  }

  childIsActived(): void {
    this.childIsActive = true;
  }

  childIsDeActived(): void {
    this.childIsActive = false;
  }
}
