import {NgModule} from '@angular/core';
import {RestaurantRoutingModule} from './restaurant-routing.module';
import {RestaurantComponent} from './restaurant.component';
import {RestaurantDetailComponent} from './restaurant-detail/restaurant-detail.component';
import {RestaurantEditComponent} from './restaurant-edit/restaurant-edit.component';
import {AdvertisementEditComponent} from './advertisement-edit/advertisement-edit.component';
import {OpeninghoursEditComponent} from './openinghours-edit/openinghours-edit.component';
import {RestauranttableEditComponent} from './restauranttable-edit/restauranttable-edit.component';
import {RestaurantOrdersComponent} from './restaurant-orders/restaurant-orders.component';
import {ReservationListComponent} from '../reservation-list/reservation-list.component';
import {SalesComponent} from '../sales/sales.component';
import {ReservationComponent} from '../reservation/reservation.component';
import {MenuItemEditComponent} from './menu-item-edit/menu-item-edit.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    RestaurantComponent,
    RestaurantDetailComponent,
    RestaurantEditComponent,
    AdvertisementEditComponent,
    OpeninghoursEditComponent,
    RestauranttableEditComponent,
    RestaurantOrdersComponent,
    ReservationListComponent,
    SalesComponent,
    ReservationComponent,
    MenuItemEditComponent,
  ],
  imports: [
    RestaurantRoutingModule, SharedModule],
})
export class RestaurantModule {
}
