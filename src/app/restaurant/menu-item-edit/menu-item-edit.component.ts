/**
 * Created by "Dorien & Nick" on 16/08/2021.
 */
import {Component, OnInit} from '@angular/core';
import {Restaurant} from '../../core/model/restaurant';
import {User} from '../../core/model/user';
import {MenuItem} from '../../core/model/menuItem';
import {Menu} from '../../core/model/menu';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {RestaurantService} from '../../core/service/restaurant.service';
import {TokenStorageService} from '../../core/service/token-storage.service';
import {MenuItemService} from '../../core/service/menu-item.service';
import {AbstractControl, FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {DialogService} from '../../shared/dialog/dialog.service';

@Component({
  selector: 'app-menu-item-edit',
  templateUrl: './menu-item-edit.component.html',
  styleUrls: ['./menu-item-edit.component.css']
})
export class MenuItemEditComponent implements OnInit {

  form: FormGroup;

  restaurant: Restaurant = new Restaurant();

  editIsClicked = false;
  addMenuItemClicked = false;

  isSuccessful = false;
  isFailed = false;
  errorMessage: '';

  error = false;

  currentUser: User;

  currentUserRoles: string[];

  showOrder = false;
  showEdit = false;
  showMenu = false;

  menuItems: MenuItem[] = [];

  menuItem: MenuItem = new MenuItem();

  foodTypes = ['DRINK', 'FOOD'];
  menuItemList = [];

  menu: Menu = new Menu();

  constructor(private route: ActivatedRoute,
              private location: Location,
              private restaurantService: RestaurantService,
              private tokenStorage: TokenStorageService,
              private menuItemService: MenuItemService,
              private dialogService: DialogService
  ) {
  }


  ngOnInit(): void {
    this.initForm();

    // loading restaurant
    const restaurantId = Number(this.route.snapshot.paramMap.get('id'));
    this.restaurantService.getRestaurantById(restaurantId).subscribe(
      restaurant => {
        this.restaurant = restaurant;
      }, error => {
        this.error = true;
        this.errorMessage = error.error.message;
      },
      () => {
        this.setPermission();
      }
    );

    // loading menu items
    this.menuItemService.findAllMenuItemsByRestaurantId(restaurantId).subscribe(
      menuItems => {
        this.menuItems = menuItems.menuItemCollection;
      }
    );

    // adding menu id to menu
    this.menu.id = restaurantId;
    this.menuItem.menu = this.menu;
  }

  onSubmit(): void {
    console.log(this.form);
    for (const item of this.form.value.menuItems) {
      this.menuItemList.push(item.name + ' ' + item.foodType + ' ' + item.price);
    }
    console.log(this.menuItemList);
    this.dialogService.confirmDialog({
      title: 'Do you want to add/edit these menu items?',
      message: this.menuItemList,
      confirmCaption: 'Yes',
      cancelCaption: 'No',
    }).subscribe((yes) => {
      if (yes) {
        this.menuItemService.saveMenuItems(this.form.value.menuItems).subscribe({
          next: () => this.isSuccessful = true,
          error: error => this.errorMessage = error.error.message,
          complete: () => this.ngOnInit()
        });
      }
    });
  }

  private setPermission(): void {
    // loading user
    const user = this.tokenStorage.getUser();
    this.currentUserRoles = user.roles;
    this.currentUser = user;
    if (this.currentUserRoles) {
      this.showOrder = this.currentUserRoles.includes('ROLE_ADMIN');
      this.showOrder = this.currentUserRoles.includes('ROLE_MODERATOR');
      this.showOrder = this.currentUserRoles.includes('ROLE_USER');
      this.showMenu = this.currentUserRoles.includes('ADMIN');
      this.showMenu = this.currentUserRoles.includes('ROLE_MODERATOR');
      this.showMenu = this.currentUserRoles.includes('ROLE_USER');

      this.showEdit = this.currentUserRoles.includes('ROLE_ADMIN');

      if (this.currentUserRoles.includes('ROLE_MODERATOR') && this.showEdit === false) {
        this.showEdit = this.restaurant.owner.id === user.id;
      }
    }
  }

  goBack(): void {
    this.location.back();
  }

  editMenuItem(menuItem: MenuItem): void {
    (this.form.get('menuItems') as FormArray).push(
      new FormGroup({
        id: new FormControl(menuItem.id, [Validators.required]),
        name: new FormControl(menuItem.name, [Validators.required]),
        price: new FormControl(menuItem.price, [Validators.required]),
        foodType: new FormControl(menuItem.foodType, [Validators.required]),
        vegetarian: new FormControl(menuItem.vegetarian, [Validators.required]),
        menu: new FormControl(menuItem.menu, [Validators.required]),
      })
    );
  }

  deleteMenuItem(id: number): void {
    if (confirm('Are ya sure?!')) {
      this.menuItemService.deleteMenuItemById(id).subscribe(
        value => {
          alert('Menuitem deleted!');
        },
        error => {
          this.errorMessage = error.error.message;
        },
        () => {
          window.location.reload();
        }
      );
    }

  }

  private initForm(): void {
    this.form = new FormGroup({
      menuItems: new FormArray([]),
    });
    console.log(this.form);
  }

  onAddMenuItem(): void {
    this.addMenuItemClicked = true;
    (this.form.get('menuItems') as FormArray).push(
      new FormGroup({
        id: new FormControl(null),
        name: new FormControl(null, [Validators.required]),
        price: new FormControl(null, [Validators.required]),
        foodType: new FormControl(null, [Validators.required]),
        vegetarian: new FormControl(false, [Validators.required]),
        menu: new FormControl(this.menu, [Validators.required]),
      })
    );
    console.log((this.form.get('menuItems') as FormArray).length);
  }

  get menuItemControls(): AbstractControl[] {
    return (this.form.get('menuItems') as FormArray).controls;
  }

  onCancelAddMenuItem(index: number): void {
    (this.form.get('menuItems') as FormArray).removeAt(index);
  }
}
