/**
 * Created by "Andy Van Camp" on 10/08/2021.
 */
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {RestaurantService} from '../../core/service/restaurant.service';
import {Restaurant} from '../../core/model/restaurant';
import {TokenStorageService} from '../../core/service/token-storage.service';
import {User} from '../../core/model/user';
import {Address} from '../../core/model/address';
import {AddressService} from '../../core/service/address.service';

@Component({
  selector: 'app-restaurant-edit',
  templateUrl: './restaurant-edit.component.html',
  styleUrls: ['./restaurant-edit.component.css']
})
export class RestaurantEditComponent implements OnInit {

  currentUser: any;

  restaurant: Restaurant;
  ownerResto: User = new User();

  address: Address;

  categories = ['BELGIAN', 'ITALIAN', 'GREEK', 'INDIAN', 'THAIS', 'CHINESE', 'AMERICAN', 'SPANISH', 'FRENCH', 'MEXICAN', 'BRITISH'];

  isSuccessful = false;
  isFailed = false;
  errorMessage = '';
  error = false;
  isLoaded = false;
  isNew = false;


  constructor(private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              private restaurantService: RestaurantService,
              private addressService: AddressService,
              private tokenStorage: TokenStorageService
  ) {
  }

  ngOnInit(): void {
    this.currentUser = this.tokenStorage.getUser();
    this.ownerResto.id = this.currentUser.id;

    const id = Number(this.route.snapshot.paramMap.get('id'));

    if (id === 0) {
      this.setNewRestaurant();
    } else {
      this.setUpdateRestaurant(id);
    }
  }

  setNewRestaurant(): void {
    this.restaurant = new Restaurant();
    this.restaurant.kitchen = 'BELGIAN';
    this.restaurant.owner = this.ownerResto;
    this.address = new Address();
    console.log('new restaurant: ' + JSON.stringify(this.restaurant.id));
    console.log('ownerId of new restaurant: ' + JSON.stringify(this.currentUser.id));
    this.isLoaded = true;
    this.isNew = true;
  }

  setUpdateRestaurant(id: number): void {
    this.restaurantService.getRestaurantById(id).subscribe(
      restaurant => {
        this.restaurant = restaurant;
        console.log('restaurant response edit: ' + JSON.stringify(restaurant));
      }, error => {
        this.error = true;
        this.errorMessage = error.error.message;
      },
      () => {
        this.addressService.getAddressById(id).subscribe(
          address => {
            console.log('address from DB: ' + JSON.stringify(address));
            this.address = address;
          },
          error => {
            console.log('error at address request');
            this.error = true;
            this.errorMessage = error.error.message;
          },
          () => {
            this.isLoaded = true;
          }
        );

      }
    );
  }


  onSubmit(): void {
    if (confirm('are ya sure??')) {
      // to save a restaurant
      if (this.restaurant.id === undefined) {
        this.restaurantService.saveRestaurant(this.restaurant).subscribe(
          data => {
            this.restaurant.id = data.id;
            this.address.restaurant = this.restaurant;
          },
          error => {
            this.errorMessage = error.error.message;
          },
          () => {
            this.addressService.saveAddress(this.address).subscribe(
              data => {
                this.isSuccessful = true;
              },
              error => {
                this.errorMessage = error.error.message;
              },
              () => {
                this.router.navigate(['openinghours'], {relativeTo: this.route});
              }
            );
          }
        );
      } else {
        // to update a restaurant
        this.restaurantService.updateRestaurant(this.restaurant.id, this.restaurant).subscribe(
          data => {
            console.log('restaurant response update: ' + JSON.stringify(data));
            this.isSuccessful = true;
          },
          error => {
            this.isFailed = true;
            this.errorMessage = error.error.message;
          },
          () => {
            this.addressService.updateAddress(this.address).subscribe(
              data => {
                this.isSuccessful = true;
              },
              error => {
                this.isFailed = true;
                this.errorMessage = error.error.message;
              },
              () => {
                this.router.navigate(['../'], {relativeTo: this.route});
              }
            );
          }
        );
      }
    } else {
      // window.location.reload(); to mess with people
    }
  }

  goBack(): void {
    this.location.back();
  }

  deleteResto(): void {
    if (confirm('Are ya sure?')) {
      if (confirm('Really???')) {
        this.restaurantService.deleteRestaurantById(this.restaurant.id).subscribe(
          value => {
            alert('Database Deleted!');
          },
          error => {
            this.errorMessage = error.error.message;
          },
          () => {
            this.location.back();
            this.location.back();
          }
        );
      }
    }
  }
}
