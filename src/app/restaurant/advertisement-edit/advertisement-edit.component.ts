/**
 * Created by "Andy Van Camp" on 16/08/2021.
 */
import {Component, OnInit} from '@angular/core';
import {AdvertisementService} from '../../core/service/advertisement.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {Advertisement} from '../../core/model/advertisement';
import {Restaurant} from '../../core/model/restaurant';
import {WalletService} from '../../core/service/wallet.service';
import {TokenStorageService} from '../../core/service/token-storage.service';
import {User} from '../../core/model/user';
import {Wallet} from '../../core/model/wallet';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-advertisement-edit',
  templateUrl: './advertisement-edit.component.html',
  styleUrls: ['./advertisement-edit.component.css']
})
export class AdvertisementEditComponent implements OnInit {

  form: FormGroup;

  loggedUser: User = new User();
  wallet: Wallet = new Wallet();

  advertisement: Advertisement;
  advertisementCollection: Advertisement[];
  advertisementCost: number;

  restaurant: Restaurant = new Restaurant();

  isFailed = false;
  errorMessage = '';
  error = false;
  isLoaded = false;
  adExist = false;

  constructor(private advertisementService: AdvertisementService,
              private route: ActivatedRoute,
              private location: Location,
              private walletService: WalletService,
              private tokenStorage: TokenStorageService) {
  }

  ngOnInit(): void {
    this.adExist = false;
    this.initForm();
    this.loggedUser = this.tokenStorage.getUser();
    this.getWalletUser();

    const id = Number(this.route.snapshot.paramMap.get('id'));

    this.restaurant.id = id;

    this.checkForAd();
  }

// todo update creation of advertisement to use expiration time
  onSubmit(): void {
    this.advertisementCost = this.form.value.advertisementCost;
    this.advertisement.description = this.form.value.description;
    if (confirm('are ya sure?')) {
      if (this.adExist) {
        // to update a advertisement
        this.walletService.addToWallet(this.loggedUser.id, -Math.abs(this.advertisementCost)).subscribe(
          value => {
          },
          error => {
            this.isFailed = true;
            this.errorMessage = error.error.message;
          },
          () => {
            this.advertisementService.updateAdvertisement(this.advertisement, this.advertisementCost).subscribe(
              data => {
              },
              error => {
                this.errorMessage = error.error.message;
              },
              () => {
                this.ngOnInit();
              }
            );
          }
        );
      } else {
        // to save a advertisement
        if (this.advertisement.id === undefined) {
          this.walletService.addToWallet(this.loggedUser.id, -Math.abs(this.advertisementCost)).subscribe(
            value => {
            },
            error => {
              this.isFailed = true;
              this.errorMessage = error.error.message;
            },
            () => {
              this.advertisementService.saveAdvertisement(this.advertisement, this.advertisementCost).subscribe(
                data => {
                },
                error => {
                  this.isFailed = true;
                  this.errorMessage = error.error.message;
                },
                () => {
                  this.ngOnInit();
                }
              );
            }
          );
        }
      }
    }
  }

  goBack(): void {
    this.location.back();
  }

  deleteAdvertisement(): void {
    this.advertisementService.deleteById(this.restaurant.id).subscribe({
      complete: () => this.ngOnInit()
    });
  }

  setNewAdvertisement(): void {
    this.advertisement = new Advertisement();
    this.advertisement.restaurant = this.restaurant;
    this.isLoaded = true;
  }

  // set update or new advertisement
  checkForAd(): void {
    this.advertisementService.getAllAdvertisement().subscribe({
        next: advertisementCollection => this.advertisementCollection = advertisementCollection.advertisementCollection,
        error: err => this.errorMessage = err.error.message,
        complete: () => {
          this.advertisementCollection.forEach(
            add => {
              if (add.id === this.restaurant.id) {
                this.adExist = true;
                this.isLoaded = true;
                this.advertisement = add;
                this.form.patchValue({
                  description: add.description
                });
              }
            }
          );
          if (!this.adExist) {
            this.setNewAdvertisement();
          }
        }
      }
    );
  }

  private getWalletUser(): void {
    this.walletService.getWalletById(this.loggedUser.id).subscribe(
      value => {
        this.wallet.storeCredit = value.storeCredit;
      }
    );
  }

  private initForm(): void {
    this.form = new FormGroup({
      description: new FormControl(null, [Validators.required]),
      advertisementCost: new FormControl(0, [Validators.required]),
    });
  }
}
