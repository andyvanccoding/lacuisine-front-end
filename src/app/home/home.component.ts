import {Component, OnInit} from '@angular/core';
import {AdvertisementService} from '../core/service/advertisement.service';
import {Advertisement} from '../core/model/advertisement';
import {AdItem} from './advertisement/AdItem';
import {RestaurantAdComponent} from './advertisement/restaurant-ad/restaurant-ad.component';
import {ActivatedRoute} from "@angular/router";

/**
 * Created by "Andy Van Camp" on 16/08/2021.
 */

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  advertisementList: Advertisement[] = [];
  advertisementList2: AdItem[] = [];

  imgSource = 'assets/img/default.jpg';

  constructor(private advertisementService: AdvertisementService) {
  }

  ngOnInit(): void {
    this.advertisementService.getAllAdvertisement().subscribe({
      next: data => {
        this.advertisementList = data.advertisementCollection;
      },
      complete: () => {
        this.advertisementList.forEach(value => {
          this.advertisementList2.push(new AdItem(RestaurantAdComponent, value));
        });
      }
    });
  }

  // constructor(private activatedRoute: ActivatedRoute) {
  // }
  //
  // ngOnInit(): void {
  //   this.activatedRoute.data.subscribe({
  //     next: value => this.advertisementList = value.advertisement
  //   });
  // }
}

// todo https://angular.io/guide/dynamic-component-loader load add one at a time
