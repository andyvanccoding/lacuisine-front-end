import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AdvertisementService} from '../../core/service/advertisement.service';
import {AdItem} from './AdItem';
import {RestaurantAdComponent} from './restaurant-ad/restaurant-ad.component';
import {Advertisement} from '../../core/model/advertisement';


@Injectable({
  providedIn: 'root'
})
export class AdvertisementResolver implements Resolve<Advertisement[]> {

  advertisementList: Advertisement[] = [];

  constructor(private advertisementService: AdvertisementService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    this.advertisementService.getAllAdvertisement().subscribe({
      next: data => {
        console.log(data.advertisementCollection);
        this.advertisementList = data.advertisementCollection;
      },
      complete: () => {
        return this.advertisementList;
      }
    });
  }
}

@Injectable({providedIn: 'root'})
export class AdItemResolver implements Resolve<AdItem[]> {

  advertisementList: Advertisement[] = [];
  advertisementList2: AdItem[] = [];

  constructor(private advertisementService: AdvertisementService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    this.advertisementService.getAllAdvertisement().subscribe({
      next: data => {
        console.log(data.advertisementCollection);
        this.advertisementList = data.advertisementCollection;
      },
      complete: () => {
        this.advertisementList.forEach(value => {
          this.advertisementList2.push(new AdItem(RestaurantAdComponent, value));
        });
        console.log(this.advertisementList);
        console.log(this.advertisementList2);
        return this.advertisementList2;
      }
    });
  }
}
