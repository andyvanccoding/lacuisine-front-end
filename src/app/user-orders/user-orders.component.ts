/**
 * Created by "Dorien and Nick" on 24/08/2021.
 */

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Order} from '../core/model/order';
import {OrderCollection} from '../core/model/order-collection';
import {OrderItemService} from '../core/service/order-item.service';
import {OrderService} from '../core/service/order.service';
import {Location} from "@angular/common";

@Component({
  selector: 'app-user-orders',
  templateUrl: './user-orders.component.html',
  styleUrls: ['./user-orders.component.css']
})
export class UserOrdersComponent implements OnInit {

  orderCollection: OrderCollection = new OrderCollection();
  totalPriceTemp = 0;
  totalExpenses = 0;

  errorMessage: '';

  error = false;

  constructor(private orderService: OrderService,
              private orderItemService: OrderItemService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  ngOnInit(): void {
    const userId = Number(this.route.snapshot.paramMap.get('id'));
    console.log('id from route: ' + userId);
    this.orderService.getOrderByUserId(userId).subscribe(
      orders => {
        this.orderCollection.orderCollection = orders.orderCollection;
      },
      error => {
        this.error = true;
        this.errorMessage = error.error.message;

      },
      () => {
        console.log(JSON.stringify(this.orderCollection.orderCollection));
        this.orderCollection.orderCollection.forEach(order => this.getOrderItems(order));
      }
    );
  }

  getOrderItems(order: Order): void {
    this.orderItemService.getOrderItemsByOrderId(order.id).subscribe(
      orderItems => {
        order.orderItemList = orderItems.orderItemCollection;
        orderItems.orderItemCollection.forEach(orderItem => this.totalPriceTemp += (orderItem.menuItem.price * orderItem.amount))
      },
      error => {
        this.error = true;
        this.errorMessage = error.error.message;

      },
      () => {
        console.log(JSON.stringify(this.orderCollection.orderCollection));
        console.log(this.totalPriceTemp);
        order.totalPrice = this.totalPriceTemp;
        this.totalExpenses += this.totalPriceTemp;
        this.totalPriceTemp = 0;
      }
    );
  }

  goBack(): void {
    this.location.back();
  }
}
