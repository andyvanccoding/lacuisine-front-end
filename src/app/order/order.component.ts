import {Component, OnInit} from '@angular/core';
import {OrderService} from '../core/service/order.service';
import {ActivatedRoute, Router} from '@angular/router';
import {OrderItemService} from '../core/service/order-item.service';
import {Order} from '../core/model/order';
import {OrderItem} from '../core/model/orderItem';
import {Location} from '@angular/common';
import {User} from '../core/model/user';
import {Restaurant} from '../core/model/restaurant';
import {MatDialog} from '@angular/material/dialog';
import {PaymentFolderComponent} from '../payment-folder/payment-folder.component';
import {WalletService} from '../core/service/wallet.service';
import {UserService} from '../core/service/user.service';
import {TokenStorageService} from '../core/service/token-storage.service';
import {RestaurantService} from '../core/service/restaurant.service';
import {SessionDataStorageService} from '../core/service/session-data-storage.service';
import {switchMap} from 'rxjs';
import {OrderItemCollection} from '../core/model/orderItem-collection';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  orderItems: OrderItem[];
  order: Order = new Order();
  errorMessage: '';
  restaurantName: Restaurant = new Restaurant();
  orderTotal = 0;
  currentUser: User;
  currentUserWallet: number;
  walletCalculatedAfterOrder = 0;
  isFailed = false;
  message: '';


  constructor(private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              private matDialog: MatDialog,
              private orderService: OrderService,
              private userService: UserService,
              private walletService: WalletService,
              private tokenStorageService: TokenStorageService,
              private restaurantService: RestaurantService,
              private orderItemService: OrderItemService,
              private storageService: SessionDataStorageService) {
  }

  ngOnInit(): void {
    console.log('data from storage: ', this.storageService.getCurrentorder());
    this.order = this.storageService.getCurrentorder();
    this.orderItems = this.order.orderItemList;
    this.totalCalculate();
    this.getUser();
    this.getWallet();
  }

  totalCalculate(): void {
    this.orderItems.forEach(orderItem => {
        this.orderTotal += (orderItem.amount * orderItem.menuItem.price);
      }
    );
  }

  goBack(): void {
    this.router.navigate(['../restaurant/' + this.order.restaurant.id], {relativeTo: this.route});
  }

  private getUser(): void {
    const user = this.tokenStorageService.getUser();
    this.currentUser = user;
  }

  private getWallet(): void {
    this.walletService.getWalletById(this.currentUser.id).subscribe(
      wallet => {
        this.currentUserWallet = wallet.storeCredit;
      }
    );
  }

  private calculateWalletWithOrderTotal(): void {
    this.walletCalculatedAfterOrder = this.currentUserWallet - this.orderTotal;
    this.walletService.addToWallet(this.currentUser.id, -this.orderTotal).subscribe();
    this.orderService.saveOrder(this.order)
      .pipe(
        switchMap(result => {
          this.orderItems.forEach(orderItem => orderItem.order.id = result.id);
          const orderItemCollection = new OrderItemCollection();
          orderItemCollection.orderItemCollection = this.orderItems;
          return this.orderItemService.saveOrderItems(orderItemCollection);
        })
      )
      .subscribe();
    this.storageService.clearOrder();
  }

  openPaymentForm(): void {
    if (this.currentUserWallet < this.orderTotal) {
      alert('insufficient funds');
    } else {
      this.calculateWalletWithOrderTotal();
      this.matDialog.open(PaymentFolderComponent,
        {
          data: {
            total: this.orderTotal,
            name: this.currentUser.username,
            credits: this.currentUserWallet,
            creditsAfterPayment: this.walletCalculatedAfterOrder,
            order: this.order,
          },
          width: '500px',
          height: '500px',
        });
    }
  }
}

// todo need to implement restaurant and order date-time
