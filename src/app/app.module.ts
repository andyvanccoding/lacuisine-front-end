import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {BoardAdminComponent} from './board-admin/board-admin.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {ProfileComponent} from './profile/profile.component';
import {RegisterComponent} from './register/register.component';
import {authInterceptorProviders} from './core/helpers/auth-interceptor.interceptor';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
// import {MatFormFieldModule} from '@angular/material/form-field';
import {OrderComponent} from './order/order.component';
import {NotAuthorizedComponent} from './shared/not-authorized/not-authorized.component';
import {PaymentFolderComponent} from './payment-folder/payment-folder.component';
import {UserOrdersComponent} from './user-orders/user-orders.component';
import {EditListConfirmComponent} from './restaurant/menu-item-edit/edit-list-confirm/edit-list-confirm.component';
import {ConfirmComponent} from './shared/dialog/confirm/confirm.component';
import {SharedModule} from './shared/shared.module';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { RestaurantAdComponent } from './home/advertisement/restaurant-ad/restaurant-ad.component';
import {AdItemResolver, AdvertisementResolver} from "./home/advertisement/advertisement-resolver.service";


@NgModule({
  declarations: [
    AppComponent,
    BoardAdminComponent,
    HomeComponent,
    LoginComponent,
    ProfileComponent,
    RegisterComponent,
    PageNotFoundComponent,
    OrderComponent,
    NotAuthorizedComponent,
    PaymentFolderComponent,
    UserOrdersComponent,
    EditListConfirmComponent,
    ConfirmComponent,
    RestaurantAdComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    AppRoutingModule,
  ],
  providers: [authInterceptorProviders, AdvertisementResolver, AdItemResolver],
  bootstrap: [AppComponent]
})
export class AppModule {
}
