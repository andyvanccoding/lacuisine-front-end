import {Component, Inject, OnInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Location} from '@angular/common';
import {User} from '../core/model/user';
import {UserService} from '../core/service/user.service';
import {Wallet} from '../core/model/wallet';
import {WalletService} from '../core/service/wallet.service';
import {TokenStorageService} from '../core/service/token-storage.service';

@Component({
  selector: 'app-payment-folder',
  templateUrl: './payment-folder.component.html',
  styleUrls: ['./payment-folder.component.css']
})
export class PaymentFolderComponent implements OnInit {
  constructor(private location: Location,
              private walletService: WalletService,
              private userService: UserService,
              private tokenStorage: TokenStorageService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public matDialogRef: MatDialogRef<PaymentFolderComponent>){}

  ngOnInit(): void {
  }
  goBack(): void {
    this.location.back();
}
  close(): void {
    this.matDialogRef.close();
  }
}
