import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from 'src/app/core/service/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class ShowOrderGuard implements CanActivate {

  constructor(private tokenStorage: TokenStorageService,
              private router: Router){

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    console.log(this.tokenStorage.getOrder());

    if (!this.tokenStorage.getOrder()){
      return this.router.parseUrl('/notauthorized');
    } else {
      return true;
    }
    
  }
  
}
