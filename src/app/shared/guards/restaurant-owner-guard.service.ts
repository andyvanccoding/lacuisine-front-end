/**
 * Created by "Dorien and Nick" on 16/08/2021.
 */

import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {User} from '../../core/model/user';
import {TokenStorageService} from '../../core/service/token-storage.service';
import {RestaurantService} from '../../core/service/restaurant.service';
import {Restaurant} from '../../core/model/restaurant';


@Injectable({
  providedIn: 'root'
})
export class RestaurantOwnerGuard implements CanActivate {

  currentUser: User;
  restaurant: Restaurant;

  constructor(private tokenStorage: TokenStorageService,
              private restaurantService: RestaurantService,
              private router: Router) {
    this.currentUser = this.tokenStorage.getUser();
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (!this.tokenStorage.getToken()) {
      return this.router.createUrlTree(['/notauthorized']);
    }
    const restaurantId = Number(route.paramMap.get('id'));
    return new Observable(subscriber => {
      this.restaurantService.getRestaurantById(restaurantId).subscribe({
        next: restaurant => this.restaurant = restaurant,
        error: err => {
          return this.router.createUrlTree(['/pagenotfound404']);
        },
        complete: () => {
          if (this.tokenStorage.getUser().roles.includes('ROLE_ADMIN') || this.currentUser && this.restaurant.owner.id === this.currentUser.id) {
            subscriber.next(true);
            subscriber.complete();
          } else {
            subscriber.next(this.router.createUrlTree(['../notauthorized']));
            subscriber.complete();
          }
        }
      });
    });
  }
}
