/**
 * Created by "Dorien" on 30/08/2021.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from 'src/app/core/service/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedInGuard implements CanActivate {

  constructor(private tokenStorage: TokenStorageService,
              private router: Router){

              }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
     
     
    if(!this.tokenStorage.getToken()){
      return this.router.navigateByUrl('/notauthorized');
    } else {
      return true;
    }
  }
  
}
