/**
 * Created by "Dorien and Nick" on 27/08/2021.
 */

import {Injectable} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import {Observable} from 'rxjs';
import {User} from 'src/app/core/model/user';
import {TokenStorageService} from 'src/app/core/service/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

  currentUser: User = new User();

  constructor(private tokenStorage: TokenStorageService,
              private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (!this.tokenStorage.getToken()) {
      return this.router.navigateByUrl('/notauthorized');
    }
    console.log(!!this.tokenStorage.getUser())
    return !!this.tokenStorage.getUser() || this.router.navigateByUrl('/notauthorized');
  }

}
