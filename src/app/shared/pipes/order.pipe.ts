import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'order'
})
export class OrderPipe implements PipeTransform {

  transform(value: any, prop: string, type: string): any {
    switch (type) {
      case 'string':
        return this.sortString(value, prop);
        break;
      case 'number':
        return this.sortNumber(value, prop);
        break;
      default:
        console.log(`Something went terribly wrong!`);
    }


  }

  private sortString(value: any, prop: string): any {
   return value.sort((a, b) => {
      const nameA = a[prop].toUpperCase(); // ignore upper and lowercase
      const nameB = b[prop].toUpperCase(); // ignore upper and lowercase
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }

      // names must be equal
      return 0;
    });
  }

  private sortNumber(value: any, prop: string): any {
    return value.sort((a, b) => {
      return a[prop] - b[prop];
    });
  }

}
