import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AdDirective} from '../../directive/ad.directive';
import {AdComponent} from '../ad-component';
import {AdItem} from '../../../home/advertisement/AdItem';

@Component({
  selector: 'app-ad-banner',
  template: `
    <div class="align-items-center">
      <h3>Advertisements</h3>
      <ng-template appAd></ng-template>
    </div>
  `,
  styleUrls: ['./ad-banner.component.css']
})
export class AdBannerComponent implements OnInit, OnDestroy {

  @Input() ads: AdItem[] = [];

  currentAdIndex = -1;

  @ViewChild(AdDirective, {static: true}) adHost!: AdDirective;
  interval: number | undefined;

  ngOnInit(): void {
    this.loadComponent();
    this.getAds();
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  loadComponent(): void {
    this.currentAdIndex = (this.currentAdIndex + 1) % this.ads.length;
    const adItem = this.ads[this.currentAdIndex];

    const viewContainerRef = this.adHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent<AdComponent>(adItem.component);
    componentRef.instance.data = adItem.data;
  }

  getAds(): void {
    this.interval = setInterval(() => {
      this.loadComponent();
    }, 3000);
  }

}
